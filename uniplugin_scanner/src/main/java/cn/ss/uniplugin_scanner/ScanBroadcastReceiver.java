package cn.ss.uniplugin_scanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.device.ScanManager;

/**
 * Created by hpb
 * on 2021/12/30 14:54
 */
public class ScanBroadcastReceiver extends BroadcastReceiver {
    ScanCallback callback;

    public ScanBroadcastReceiver(ScanCallback callback){
        this.callback = callback;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if(ScanManager.ACTION_DECODE.equals(intent.getAction())){
            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int length = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            String result = new String(barcode, 0, length);
            callback.onResult(result);
        }
    }
}
