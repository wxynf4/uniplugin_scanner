package cn.ss.uniplugin_scanner;

/**
 * Created by hpb
 * on 2021/12/30 15:57
 */
public interface ScanCallback {
    void onResult(String result);
}
