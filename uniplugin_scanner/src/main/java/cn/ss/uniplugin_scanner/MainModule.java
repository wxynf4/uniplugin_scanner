package cn.ss.uniplugin_scanner;

import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.android.decode.BarcodeManager;
import com.android.decode.DecodeResult;
import com.android.decode.ReadListener;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniDestroyableModule;

/**
 * Created by hpb
 * on 2021/12/30 14:47
 */
public class MainModule extends UniDestroyableModule implements ReadListener, ScanCallback {
    //优博讯
    ScanManager scanManager;
    ScanBroadcastReceiver scanBroadcastReceiver;

    //MoByData
    private BarcodeManager mBarcodeManager;

    UniJSCallback callback;
    String deviceType;

    @UniJSMethod(uiThread = true)
    public void show(JSONObject object){
        Toast.makeText(mUniSDKInstance.getContext(), object.getString("msg"), Toast.LENGTH_LONG).show();
    }

    @UniJSMethod(uiThread = false)
    public void register(JSONObject object, UniJSCallback callback){
        deviceType = object.getString("deviceType");
        this.callback = callback;
        if("1".equals(deviceType)){
            initURoVo();
        }
        if("2".equals(deviceType)){
            initMoByData();
        }
    }

    @UniJSMethod(uiThread = false)
    public void unRegister(){
        callback = null;
        if("1".equals(deviceType)){
            if(scanManager != null){
                scanManager.closeScanner();
                scanManager.stopDecode();
                scanManager = null;
                if(scanBroadcastReceiver != null){
                    mUniSDKInstance.getContext().unregisterReceiver(scanBroadcastReceiver);
                    scanBroadcastReceiver = null;
                }
            }
        }
        if("2".equals(deviceType)){
            if(mBarcodeManager != null){
                mBarcodeManager.stopDecode();
                mBarcodeManager.removeReadListener(this);
                mBarcodeManager.release();
                mBarcodeManager = null;
            }
        }
    }

    //初始化MoByData
    private void initMoByData(){
        mBarcodeManager = new BarcodeManager();

        //设置扫码结果后缀为空
        Intent mSendIntent;
        mSendIntent = new Intent("com.android.action.setPropertyString");
        mSendIntent.putExtra("PropertyID", 0x0027);
        // 广播action地址改成 com.barcode.sendBroadcast
        mSendIntent.putExtra("PropertyString", "");
        mUniSDKInstance.getContext().sendBroadcast(mSendIntent);

        mBarcodeManager.isInitialized();
        mBarcodeManager.addReadListener(this);
    }

    //初始化优博讯
    private void initURoVo(){
        scanManager = new ScanManager();
        scanManager.switchOutputMode(0);
        scanManager.openScanner();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.ACTION_DECODE_DATA");
        scanBroadcastReceiver = new ScanBroadcastReceiver(this);
        mUniSDKInstance.getContext().registerReceiver(scanBroadcastReceiver, intentFilter);
    }

    //MoByData扫码回调
    @Override
    public void onRead(DecodeResult decodeResult) {
        JSONObject object = new JSONObject();
        object.put("result", decodeResult.getText().replace("\n", ""));
        callback.invokeAndKeepAlive(object);
    }

    //优博讯扫码回调
    @Override
    public void onResult(String result) {
        JSONObject object = new JSONObject();
        object.put("result", result);
        callback.invokeAndKeepAlive(object);
    }

    @Override
    public void destroy() {
        unRegister();
    }
}
