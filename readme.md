# 源码地址https://gitee.com/NF-Bo/uniplugin_scanner

# 使用说明

# `此插件用于优博讯（Urovo）PDA扫描头扫码和摩比（MoByData）PDA扫描头扫码`

## 引用方式

`const scanner = uni.requireNativePlugin('bo-plugin-scanner');`

## 插件接口

#### `register(options, callback)`

#### `unRegister()`

## 使用说明

### `register(options, callback)`参数说明

| 参数     |   类型   | 参数说明 |
| -------- | :------: | -------: |
| options  |   json   | 参数配置 |
| callback | function | 回调方法 |

### register->options{object}

| 参数     |  类型  | 是否可空 |                                                            参数说明 |
| --------       | :----: | -------- | ------------------------------------------------------------------: |
| deviceType     | string | 否       |                             传入'1'表示使用优博讯扫码，传入'2'表示使用摩比扫码 |


### register->callback

| 参数   |  类型   | 参数说明 |
| ------ | :-----: | -------: |
| result | Boolean | 扫描头返回的结果 |

### `unRegister()`说明

销毁注册的广播，关闭扫描头，停止解码。建议将此方法写入到onHide、onUnload生命周期中

